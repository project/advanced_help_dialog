Advanced Help Dialog Example

This module provides example help dialog link on the front page. It will link to
the advanced help topic for the Advanced Help Dialog module.

Make sure you set the following permissions appropriately:

View advanced help dialog links (Advanced Help Dialog)
View help topics (Advanced Help)